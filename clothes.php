<?php
$jsonurl = "https://api.openweathermap.org/data/2.5/weather?q=breda,&appid=a67d3f36cd740475df0923cbcb7f607c";
$json = file_get_contents($jsonurl);

$weather = json_decode($json);
$kelvin = $weather->main->temp;
$celcius = $kelvin - 273.15;
$clothes = array();

if($celcius <= 20){
    echo "<div class='top'> " . " <img class='icon' src='weather.png'></div>";
    echo "<div class='bottom'>" . "<p class='colder'>" . 'Het is'. "&nbsp;" . $celcius .  "&nbsp;" . " Graden buiten"  . "</p>";
    echo "<p class='colder'>" . 'Je kan het beste dikke en warme kleren aantrekken' . "</p> . </div>";
} else if($celcius >= 21){
    echo "<div class='top'> " . "<img class='icon' src='sun-131979013495145260.png'>" . "</div>";
    echo "<div class='bottom'>" . "<p class='warmer'>" . 'Het is' ."&nbsp;". $celcius . "&nbsp;" .  "Graden buiten"  . "</p>";
    echo "<p class='warmer'>" . 'Het is verstandig om korte, dunne kleding aan te trekken en genoeg water te drinken' . "</p>" . "</div>";
}